| **Spike ID** | MG1 | **Time Needed** | 2 Days |
|--------------|-----|-----------------|--------|

| **Title**           | MG1 � Replication (Blueprint)                                         |
|---------------------|-----------------------------------------------------------------------|
| **Personnel**       | Mathew Ponting (blueprogrammer)                                       |
| **Repository Link** | <https://bitbucket.org/blueprogrammer/mg1-replicating-with-blueprint> |

Introduction
============

In the first of 5 spikes, the user building on top of the blueprint
3<sup>rd</sup> Person template will create a multiplayer version of it
with the addition of throwing bombs, scoring and updating interfaces on
a player controller. This spike demonstrates and introduces the pure
basics of networking (allowing local and online play) in Unreal Engine
4.

TRT (Technology, Resources and Tools)
=====================================

Usually a reference of tools that are needed to be used for the
completion of this spike.

| **Resource**                                                                                | **Needed/Recommended** |
|---------------------------------------------------------------------------------------------|------------------------|
| <https://gafferongames.com/post/what_every_programmer_needs_to_know_about_game_networking/> | Recommended            |
| <https://www.unrealengine.com/en-US/blog/blueprint-networking-tutorials>                    | Needed                 |

Please Read this before Starting!
=================================

Noted during this spike: There doesn�t seem to be a place online that
explains the PlayerState well enough so please note these facts before
starting:

A PlayerState is an actor that is created after a new player pawn and
controller is created. This is a good actor to place all information
regarding a player that all connected players need to know. In this
spike, you will use the player spike to store the player�s score and
update each of the player�s UI�s have all scores updated.

Tasks
=====

1.  Complete the Blueprint Networking Tutorial. This will give you most
    of this spike completed.

2.  Create a new Player Controller Class called (BP\_PC). Get the
    playerstate and make a IsValid node. If it is not valid, set a delay
    for 0.2 seconds and then link it to the isValid Node. This will loop
    until the PlayerState is valid.

> **Creating PlayerState**

1.  Create a new Blueprint class for a PlayerState. Make sure your
    gamemode has reference this as well.

2.  Make reference to the Player Controller (BP\_PC). Make 2 integer
    variables called score and player number.

    1.  Set Player Number to Replicated and Score to RepNotify. We will
        come back later to the Score�s RepNotify Function when Updating
        all GUIs.

3.  InBeginPlay for this, get all PlayerStates in the world and for
    each, plus 1 to the player number. This will give the PlayerState
    an ID.

> **Creating Scores**

1.  Create a new blueprint that will be the score. Make it rotate and
    make it look pretty. Make sure as well that it is replicated (and
    also the movement is replicated).

2.  On Component begin overlap, make sure that the other actor has a
    class of the player pawn.

3.  Get the PlayerState and add 1 to the score. This will trigger the
    RepNotify Function of score which will be modified in Part 4.

4.  Make a new emitter that is an explosion. Then make it�s Max Draw
    Distance equal to 1000 which will make players who are far away not
    see the particle system but close players will see the new particle
    system then delete the actor.

5.  Throughout the level, place waypoints where you want scores
    to spawn. For cleanliness, you may want to include a tag for them
    called score.

6.  In the Level Blueprints begin play script, Get all the waypoints in
    world and and pick a number between 1 and the number of scores in
    the world.

7.  Pick that many waypoints and spawn a score actor on the location of
    that waypoint.

> **Creating Player GUIs**

1.  Create a new GUI file and add to viewport on BP\_PC�s
    BeginPlay script. Make sure to assign it to a variable. **DO NOT
    REPLICATE THIS.**

2.  Create 2 text fields that holds Player 1: and the other one has the
    number 0. Repeat this for up to 4 players.

3.  Make a function in the GUI that has 2 integers that reference the
    score and the player id. Using these, make an update to the score
    text to the player�s score who equals the player id.

> **Updating all GUIs**

1.  Make a for each loop in the RepNotify for the score variable in the
    player state that checks all the Player Controllers in the world and
    if they are NOT a local controller, update the GUI for each
    controller by passing in the score and the ID of the player who
    just scored.

**  
**

> **Spawn Points**

1.  In the Game Mode, override the Choose Player Start function. Make 2
    new arrays called Spawn Points and Used Spawn Points.

2.  Select a random entry in the list of Spawn Points and check if it is
    in the Used Spawn Points Array. If it is, remove that spawn point
    from the Spawn Points list and re-do the random selection (this
    loops until either a Player Start has been found or non has
    been found).

3.  If a player Start is found, add it to the used spawn points list and
    then return it as the player start that has been selected.

4.  If not, that means the server is full. You can go ahead and make
    this a spectator pawn or delete them off the server (your choice).

Discovered
==========

-   There are 2 different types of connections when it comes to unreal:

    -   **Server:** A Server acts as a central point for clients
        to connect. This holds all the information about the current
        world being broadcasted to itself and connected clients.

        -   **Dedicated Server:** A Dedicated Server essentially allows
            the server to be dedicated to just the central point and
            nothing else.

    -   **Client:** A connected user to a server is a client. This
        usually receives the information from the server that needs to
        be shown or known to the player connect to that server.

-   Replication in Unreal Engine means that all clients that are
    connected to a server will see that Replicated Actor or get the
    knowledge of a function that is being replicated.

-   You can check if a Player Controller is local or server side by
    using the Is Local Controller Checker. This comes in handy when
    updating Player HUDs or you need to disable movement for one player.

-   Has Authority (Switch or Function) will make sure that the function
    that is calling is allowed to run this function and may only run
    it once. Not having this checker may result in the code being called
    multiple times.

    -   Most likely this will be the server that has authority but note:
        **THIS IS NOT ALWAYS THE CASE.** It will usually be the aspect
        that triggered it.

-   PlayerStates are part of a player. They hold information and can run
    functions of a player that needs to be shown to all players on
    the server. This may include the players name, scores.

-   Player Controllers only replicate to the owner client�s machine and
    not to anyone else but it can be found on the server.

-   Player States can be replicated across all machines.

-   Replicating variables is simple. Replicated Variables update in the
    final part of a server tick. If the variable value has not changed
    from its original value from the beginning of a tick to the end, it
    will not be shown to the clients.

-   RepNotify allows response time to a function. RepNotify a variable
    will create a new function that is called when the value of that
    variable is changed.

-   You can also Replicate a function but in addition you can make those
    function do the following:

    -   **Multi-Cast:** First casts on the server, then the
        connected clients.

    -   **Owning Client:** On the client that executed the command.

    -   **Server-Only:** Only executed on the server.

-   You can additionally make a function reliable which in the events of
    a server lag, it can be skipped. Usually functions that do not have
    this need to be done otherwise it will break the game.

-   In unreal blueprint, there are a total of 3 symbols when working
    with networking in this engine:

    -   Tower with Thunderbolt: Only runs if the server is running the
        current function

    -   Monitor with Thunderbolt: Runs on Clients. Usually only for
        cosmetic effect.

    -   2 Balls (Usually on a variable): Replicated.

-   In the world outliner, there is a View Options with an eye and there
    is a sub menu called Choose World. Choosing the server world will
    show what is being broadcasted on the server and choosing a client
    world will show what is being shown to the client.

-   Max Draw Distance allows the use of network relevance for actors.

Open Issues and Risks
=====================

Issues
------

-   There is no �correct way� to tell which player state, player
    controller or pawn belongs to what without the use of variables set
    at the beginning of the code. In this code, I set the IDs of both
    the player state and player controller on the Event Begin Play or
    their construction scripts but this way: its more random and more
    undecided which can cause problems down the road. Sure, I get no
    problems when I test it but it can open the door for problems down
    the line.

Recommendations
===============

-   If possible, on the server side: Set both the Player Controller and
    Player State to have Player IDs. Though Player State already has a
    Player ID within it, it�s not easy to understand. Usually set this
    to the player number that entered the server. Try avoiding making it
    random as this spike showed.

-   Use the View Options to select what world you want to see. You will
    most of the time want to use the server world view but sometimes the
    client�s will come in handy as well.

-   Reliable means that this function must eventually be called. If a
    client doesn�t get the function, the server will call it again until
    the client gets it.

